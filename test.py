from unittest import TestCase
from main import binary_new

class TestBinaryNew(TestCase):
    def test_binary_new(self):
        value = binary_new()
        self.assertEqual(1, value)